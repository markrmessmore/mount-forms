import Vue        from 'vue'
import Router     from 'vue-router'
import authGuard  from './authguard.js'
import store      from './store.js'

// BASE COMPONENTS
import Admin      from '@/components/admin'
import Contact    from '@/components/contact'
import confirmNew from '@/components/formComponents/confirmNew'
import Disabled   from '@/components/disabled'
import Home       from '@/components/home'
import Login      from '@/components/login'
import More       from '@/components/more'
import New        from '@/components/new'
import Privacy    from '@/components/appComponents/privacy'
import Review     from '@/components/review'
import Upload     from '@/components/upload'

// ADMIN COMPONENTS
import About      from '@/components/adminComponents/about'
import Settings   from '@/components/adminComponents/settings'
import Social     from '@/components/adminComponents/social'
import Submitted  from '@/components/adminComponents/submitted'
import Users      from '@/components/adminComponents/users'

// FORM COMPONENTS

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/Admin',
      name: 'admin',
      component: Admin,
      beforeEnter: authGuard
    },
    {
      path: "/Calendar",
      beforeEnter() {window.open(store.getters.getSettingsCalendar.value, '_blank')}
    },
    {
      path: '/confirmNew',
      name: 'confirmNew',
      component: confirmNew
    },
    {
      path: '/Contact',
      name: 'contact',
      component: Contact
    },
    {
      path: '/Disabled',
      component: Disabled
    },
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/Login',
      name: 'login',
      component: Login
    },
    {
      path: '/More',
      name: 'more',
      component: More
    },
    {
      path: '/New',
      name: 'new',
      component: New
    },
    {
      path: '/Privacy',
      name: 'privacy',
      component: Privacy
    },
    {
      path: '/Review',
      name: 'review',
      component: Review
    },
    {
      path: '/Upload',
      name: 'upload',
      component: Upload
    },
    // ADMIN COMPONENTS
    {
      path: '/Users',
      name: 'users',
      component: Users,
      beforeEnter: authGuard
    },
    {
      path: '/Settings',
      name: 'settings',
      component: Settings,
      beforeEnter: authGuard
    },
    {
      path: '/Social',
      name: 'social',
      component: Social,
      beforeEnter: authGuard
    },
    {
      path: '/Submitted',
      name: 'submitted',
      component: Submitted,
      beforeEnter: authGuard
    },
    {
      path: '/About',
      name: 'about',
      component: About,
      beforeEnter: authGuard
    },
  ]
})
