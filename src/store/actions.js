import appdata  from '@/appdata.js'
import axios    from 'axios'
import router   from '@/router.js'
import sgMail   from '@sendgrid/mail'

// FIREBASE
import * as firebase from "firebase/app"
import "firebase/auth"
import "firebase/functions"
import "firebase/firestore"
import "firebase/storage"
firebase.initializeApp(appdata.firebase);

export default {
  addSetting({commit}, payload){
    commit('setLoading', true)
    firebase.firestore().collection('Settings').add(payload)
    .then(() => {
      router.push('/Admin')
      commit('setToast', {status: true, msg: `A listing has been added for ${payload.item}`})
      commit('setLoading', false)
    })
    .catch(err => {
      commit('setToast', {status: true, msg: err})
      commit('setLoading', false)
    })
  },
  changeUserStatus({commit}, payload){
    commit('setLoading', true)
    firebase.firestore().collection('Users').doc(payload.id).update({
      email : payload.email,
      status: payload.status
    })
    .then(() => {
      commit('setToast', {status: true, msg: `User ${payload.email} has been ${payload.status ? 'enabled' : 'disabled'}.`})
      commit('setLoading', false)
    })
    .catch(err => {
      commit('setToast', {status: true, msg: err})
      commit('setLoading', false)
    })
  },
  createUser({commit}, payload){
    let userData = {
      email   : payload.email,
      status  : true
    }
  commit('setLoading', true)
    firebase.auth().createUserWithEmailAndPassword(payload.email, payload.pass)
    .then(res => {
      firebase.firestore().collection('Users').add(userData)
      .then(()=> {
        commit('setToast', {status: true, msg: `User ${payload.email} has been created.`})
        commit('setLoading', false)
      })
    })
    .catch(err => {
      commit('setToast', {status: true, msg: err})
      commit('setLoading', false)
    })
  },
  deleteSetting({commit}, payload){
    commit('setLoading', true)
    firebase.firestore().collection('Settings').doc(payload.id).delete()
    .then(() => {
      router.push('/Admin')
      commit('setToast', {status: true, msg: `${payload.item} has been deleted.`})
      commit('setLoading', false)
    })
    .catch(err => {
      commit('setToast', {status: true, msg: err})
      commit('setLoading', false)
    })
  },
  deleteSubmission({commit}, payload){
    commit('setLoading', true)
    firebase.firestore().collection('Submissions').doc(payload.id).delete()
    .then(res => {
      commit('setToast', {status: true, msg: `Submission ${payload.title ? payload.title : payload.name} has been deleted.`})
      commit('setLoading', false)
    })
    .catch(err => {
      commit('setToast', {status: true, msg: err})
      commit('setLoading', false)
    })
  },
  getSubmitted({commit}){
    commit('setLoading', true)
    firebase.firestore().collection('Submissions').onSnapshot(allSubs => {
      let submissions = []
      allSubs.docs.forEach(submission => {
        if (submission.data().to ==  undefined){
          let currentSubmission = submission.data()
          currentSubmission.id = submission.id
          submissions.push(currentSubmission)
        }
      })
      commit('setSubmissions', submissions)
      commit('setLoading', false)
    })
  },
  getSettings({commit}){
    commit('setLoading', true)
    firebase.firestore().collection('Settings').onSnapshot(allSettings => {
      let dbSettings = []
      allSettings.forEach(item => {
        let itemData  = item.data()
        itemData.id   = item.id
        dbSettings.push(itemData)
      })
      commit('setSettings', dbSettings)
      commit('setLoading', false)
    })
  },
  getUsers({commit}){
    commit('setLoading', true)
    firebase.firestore().collection('Users').onSnapshot(allUsers => {
      let userArray = []
      allUsers.forEach(user => {
        let userData = {
          id      : user.id,
          email   : user.data().email,
          status  : user.data().status
        }
        userArray.push(userData)
      })
      commit('setUserList', userArray)
      commit('setLoading', false)
    })
  },
  login({commit}, payload){
    commit('setLoading', true)
    firebase.auth().signInWithEmailAndPassword(payload.email, payload.pass)
    .then(res => {
      commit('setUser', res.user.email)
      router.push('/Admin')
      commit('setToast', {status: true, msg: "You have been successfully logged in."})
      commit('setLoading', false)
    })
    .catch(err => {
      commit('setToast', {status: true, msg: err})
      commit('setLoading', false)
    })
  },
  logout({commit}, payload){
    commit('setLoading', true)
    firebase.auth().signOut()
    .then(res => {
      commit('setUser', null)
      router.push('/Login')
      commit('setToast', {status: true, msg: "You have been successfully logged out."})
      commit('setLoading', false)
    })
    .catch(err => {
      commit('setToast', {status: true, msg: err})
      commit('setLoading', false)
    })
  },
  resetPass({commit}, payload){
    commit('setLoading', true)
    firebase.auth().sendPasswordResetEmail(payload)
    .then(res => {
      commit('setToast', {status: true, msg: `${payload} has been sent an email to reset his/her password.`})
      commit('setLoading', false)
    })
    .catch(err => {
      commit('setToast', {status: true, msg: err})
      commit('setLoading', false)
    })
  },
  saveUploadedFile({commit}, payload){
    let storageRef  = firebase.storage().ref()
    let uploadTask  = storageRef.child(payload.name).put(payload)
    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, snapshot => {
      var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100
      commit('setProgress', {status: true, pct: progress})
    },
    error => {
      commit('setProgress', {status: false, pct: null})
      commit('setToast', {status: true, msg: error})
    }, 
    function() {
      uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
        commit('setProgress', {status: false, pct: null})
        commit('setToast', {status: true, msg: `${payload.name} has been uploaded.`})
        router.push('/')
      });
    })
  },
  saveSettings({commit}, payload){
    commit('setLoading', true)
    payload.forEach(item => {
      firebase.firestore().collection('Settings').doc(item.id).update(item)
      .then(() => {
        router.push('/Admin')
        commit('setToast', {status: true, msg: 'App settings have been updated.'})
        commit('setLoading', false)
      })
      .catch(err => {
        commit('setToast', {status: true, msg: err})
        commit('setLoading', false)
      })
    })
  },
  // sendMsg({commit, getters}, payload){
  //   commit('setLoading', true)
  //   let msgData = {
  //       from    : payload.email,
  //       name    : payload.name,
  //       // to      : getters.getSettingsEmail.value,
  //       to      : 'mark@themountcc.org',
  //       phone   : payload.phone,
  //       subject : payload.subject,
  //       text     : payload.msg
  //   }

  //   axios.post('https://us-central1-mount-forms-1b93c.cloudfunctions.net/sendEmail', msgData)
  //   .then(() => {
  //     commit('setToast', {status: true, msg: payload.successMsg})
  //     commit('setLoading', false)
  //     router.push('/')
  //   })
  //   .catch(err => {
  //     commit('setToast', {status: true, msg: err})
  //     commit('setLoading', false)
  //   })
  // },
  setProgress({commit}, payload){
    commit('setProgress', payload)
  },
  setToast({commit}, payload){
    commit('setToast', payload)
  },
  submitReq({commit, state}, payload){
    payload.dateTime  = new Date().toString().split(' GMT')[0]
    payload.status    = "Submitted"
    firebase.firestore().collection('Submissions').add(payload)
    .then(() => {
      // let successMsg  = `Your request for ${payload.title} has been submitted.`
      // {
      //     name        : payload.name,
      //     email       : [state.dbSettings.email],
      //     phone       : payload.phone,
      //     subject     : `A form for ${payload.title} has been submitted via Mount Forms Online`,
      //     msg         : `A form has been submitted by ${payload.name} for ${payload.title}. Please log in to review the information and respond to the request as appropriate.`,
          
      // }
      commit('setToast', {status: true, msg: `Your request for ${payload.title} has been submitted.`})
      router.push('/')
    })
    .catch(err => {
      let toastMsg = {
          status: true,
          msg   : err
      }
      commit('setToast', toastMsg)
    })
  },
  updateRequest({commit}, payload){
    commit('setLoading', true)
    firebase.firestore().collection('Submissions').doc(payload.id).update(payload)
    .then(() => {
      let toastMsg = {
        status: true,
        msg   : `Your changes to ${payload.title} have been saved.`
      }
      commit('setToast', toastMsg)
      commit('setLoading', false)
    })
    .catch(err => {
      let toastMsg = {
          status: true,
          msg   : err
      }
      commit('setToast', toastMsg)
      commit('setLoading', false)
    })
  }
}
