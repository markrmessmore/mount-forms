export default {
  setLoading(state, payload){
    state.loading = payload
  },
  setProgress(state, payload){
    state.progress = payload
  },
  setSettings(state, payload){
    state.dbSettings = payload
  },
  setSubmissions(state, payload){
    state.submittedRequests = payload
  },
  setToast(state, payload){
    state.toast = payload
  },
  setUser(state, payload){
    state.user = payload
  },
  setUserList(state, payload){
    state.userList = payload
  }
}
