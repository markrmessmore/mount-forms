export default {
  getAdminNav(state){
    return state.adminNav
  },
  getLoading(state){
    return state.loading
  },
  getMinistries(state){
    return state.ministryList 
  },
  getNav(state){
    return state.navList
  },
  getProgress(state){
    return state.progress
  },
  getSettings(state){
    return state.dbSettings.sort((a,b) => {
      if (a.item > b.item){
        return 1
      }
      else {
        return -1
      }
    })
  },
  getSettingsCalendar(state){
    let calSettings = {}
    state.dbSettings.forEach(setting => {
      if (setting.category == 'App' && setting.item == 'Calendar') {
        calSettings = setting
      }
    })
    return calSettings
  },
  getSettingsEmail(state){
    let emailSettings = {}
    state.dbSettings.forEach(setting => {
      if (setting.category == 'App' && setting.item == 'Email') {
        emailSettings = setting
      }
    })
    return emailSettings
  },
  getStatusOptions(state){
    return state.statusOptions
  },
  getSubmissions(state){
    return state.submittedRequests
  },
  getToast(state){
    return state.toast
  },
  getUser(state){
    return state.user
  },
  getUserList(state){
    return state.userList
  }
}
