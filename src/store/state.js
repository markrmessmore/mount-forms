export default {
  adminNav: [
    {
      item: "Users",
      icon: "fas fa-users",
      link: "/Users"
    },
    {
      item: "Form Submissions",
      icon: "fab fa-wpforms",
      link: "/Submitted"
    },
    {
      item: "App Settings",
      icon: "fas fa-cog",
      link: "/Settings"
    },
    {
      item: "About",
      icon: "fas fa-question-circle",
      link: "/About"
    },
  ],
  dbSettings: {},
  loading: false,
  ministryList: [
    'All Church', 
    "Missions", 
    "Students @ The Mount", 
    "Kids @ The Mount", 
    "Men @ The Mount",
    "Women @ The Mount",
    "Worship Ministry",
    "Staff", 
    "Leadership", 
    "Guest Services", 
    "Small Groups", 
    "Celebrate Recovery"
  ],
  navList: [
    {
      item: "Home",
      icon: "fas fa-home",
      link: "/"
    },
    {
      item: "Create Event Request",
      icon: "fas fa-plus-square",
      link: "/confirmNew",
      description: "If you are wanting to stay overnight, request the van or book an event on The Mount's campus, here is where you will begin."
    },
    {
      item: "Event Review",
      icon: "fas fa-edit",
      link: "/Review",
      description: "Need to enter in a post-event review? Go here!"
    },
    {
      item: "Mount Calendar",
      icon: "far fa-calendar-alt",
      link: "/Calendar",
      description: "Take a look at our online calendar."
    },
    {
      item: "Other Church Forms",
      icon: "fas fa-church",
      link: "/More",
      description: "Don't see what you're looking for? Check here."
    },
    {
      item: "File Upload",
      icon: "fas fa-upload",
      link: "/Upload",
      description: "Upload files here."
    },
    {
      item: "Contact Us",
      icon: "fas fa-envelope",
      link: "/Contact",
      description: "Click here to send us a lovely message."
    }
  ],
  progress: {
    status  : false,
    pct     : null
  },
  resources: [],
  statusOptions: ['Approved', 'Cancelled', 'Completed', 'Denied', 'Pending', 'Submitted'],
  submittedRequests: [],
  toast: {
    status: false,
    msg   : ""
  },
  user: null,
  userList: []
}
