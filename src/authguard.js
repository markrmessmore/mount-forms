import store  from '@/store.js'
import router from '@/router.js'
export default (to, from, next) => {
  if (store.getters.getUser) {
    // ONLY ALLOW THE USER TO ACCESS ADMIN AREAS IF MARKED AS 'ACTIVE'
    let currentUser = store.getters.getUserList.filter(user => user.email == store.getters.getUser)
    if (currentUser[0].status){
      next()
    }
    else {
      next('/Disabled')
    }
  }
  else {
    next('/Login')
  }
}
