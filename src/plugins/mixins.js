
export default {
  directives: {
  },
  data(){
    return {
      phoneMask: "(XXX) XXX - XXXX",
      rules: {
        email: [
          v => !!v || 'This field is required',
          v => /.+@.+\..+/.test(v) || 'Please enter a valid email address.'
        ],
        phone: [
          v => !!v || 'This field is required',
          v => (v && v.length == 16) || 'Please enter a valid phone number.'
        ],
        required: [
          v => !!v || 'This field is required'
        ]
      }
    }
  },
  methods: {
    submitRequest(req){
      console.log(req)
    }
  },
  computed: {

  }
}
